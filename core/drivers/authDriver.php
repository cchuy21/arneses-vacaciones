<?php
class authDriver extends driverBase {
    //función para iniciar sesión
    public static function loginauth($user = null) {
        if(!isset($_SESSION)) 
        { 
            session_start(); 
        } 
        $u = User::find_by_correo($user);
        $_SESSION['loggedin'] = true;
        $_SESSION['user_id'] = $u->id;
        return true;
    }

    public static function login($user = null, $pass = null) {
        if(!isset($_SESSION)) 
        { 
            session_start(); 
        } 
        //la sesión se realiza exitosamente
        if(isset($_SESSION['loggedin']))
            if($_SESSION['loggedin'])
                return true;
        if(is_object($user)) {
            //is_array=comprueba si la variable es un array
        } elseif(is_array($user) ) {
            $u = User::find_by_correo($user['user']);
            if(!$u) return false;
			//verifica que el password corresponda al usuario y sea correcto para iniciar sesión
            if(password_verify($user['pass'], $u->password)){
                $_SESSION['loggedin'] = true;
                $_SESSION['user_id'] = $u->id;
                return true;
            }
        } else {

        	//encontrar los usuarios por su nombre de registrado
            $u = User::find_by_correo($user);
            if(!$u) return false;
            if(password_verify($pass, $u->password)){
                $_SESSION['loggedin'] = true;
                $_SESSION['user_id'] = $u->id;
                return true;
            }
        }
        return false;
    }
    public function loginauto($user = null){
        if(!isset($_SESSION)) 
        { 
            session_start(); 
        } 
        //la sesión se realiza exitosamente
        if(isset($_SESSION['loggedin']))
            if($_SESSION['loggedin'])
                return true;
        if(is_object($user)) {
            //is_array=comprueba si la variable es un array
        } elseif(is_array($user) ) {
            $u = User::find_by_email($user['user']);
            
            if(!$u) return false;
			//verifica que el password corresponda al usuario y sea correcto para iniciar sesión
            if($u->password == md5($user['pass'])){
                $_SESSION['loggedin'] = true;
                $_SESSION['user_id'] = $u->id;
                return true;
            }
        } else {
        	//encontrar los usuarios por su nombre de registrado
            $u = User::find_by_id($user);
            if(!$u) return false;
           
                $_SESSION['loggedin'] = true;
                $_SESSION['user_id'] = $u->id;
                return true;
            
        }
        return false;
    }


 
    //aqui se obtiene el id del usuario en sesión
    public static function getUser() {
        if(!isset($_SESSION)) 
        { 
            session_start(); 
        } 
        if(isset($_SESSION['loggedin']))
            if($_SESSION['loggedin']){
            $u = User::find($_SESSION['user_id']);
            return $u;
        }
        return false;
    }
    //obtiene el usuario del sistema
    public static function getSUser() {	
        if(!isset($_SESSION)) 
        { 
            session_start(); 
        } 	
        if(isset($_SESSION['loggedin']))
            if($_SESSION['loggedin']){
        	
            $u = User::find($_SESSION['user_id']);
            return $u;
        }
        return false;
    }
    //el usuario cierra sesión
    public static function logout() {
        if(!isset($_SESSION)) 
        { 
            session_start(); 
        } 
        session_destroy();
        return true;
    }
    //se inicia sesión
    public static function isLoggedin() {
        if(!isset($_SESSION)) 
        { 
            session_start(); 
        } 	
        if(isset($_SESSION['loggedin']))
            if($_SESSION['loggedin']){
            return true;
        }
        return false;
    }
    //si el usuario no ha iniciado sesión en el sistema
    public static function chkLoggin() {
        return true;
        if( !authDriver::isLoggedin() )
            responseDriver::dispatch('E', 'No se ha iniciado sesión', 'No se ha iniciado sesión en el sistema');
    }
}