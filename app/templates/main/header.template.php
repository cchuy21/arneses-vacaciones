<?php 
    $pag = templateDriver::getData("page");
    $nombre = "";
    /*if(authDriver::isLoggedin()){
      $u = authDriver::getUser();
      $nombre = $u->nombre;
    }else{
      
    }*/
?>
<nav class="navbar navbar-expand-sm navbar-dark" id="navbar">
    <div class="container-fluid">
        <a class="navbar-brand" href="/">
            <img src="/static/images/imf.png" alt="" class="izqlogo">
        </a>

        <button class="navbar-toggler" type="button" 
        data-bs-toggle="collapse"
        data-bs-target="#navbarSupportedContent" 
        aria-controls="navbarSupportedContent" 
        aria-expanded="false" 
        aria-label="Toggle navigation">

        <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav" >
            <?php
                $paginas = array("INICIO", "NOSOTROS", "PRODUCTOS", "CONTACTO");
                foreach($paginas as $page){
                    $npage = "/".strtolower($page);
                    if($page == "INICIO"){
                        $npage = '/';
                    }
                    if($pag == $page){
                        echo '<li class="nav-item active"><a class="nav-link" href="'.$npage.'"><span>'.$page.'</span></a></li>';
                    } else {
                        echo '<li class="nav-item"><a class="nav-link" href="'.$npage.'"><span>'.$page.'</span></a></li>';
                    }
                }
            ?>
            <li class="nav-item usericn"><a class="nav-link" href="/user/login"><span><i class="bi bi-person-fill"><?php echo $nombre ?></span></a></i></li>
            <li class="nav-item lishop"><a class="nav-link" href="/tienda"><span><i class="bi bi-bag-fill"></span></a></i></li>
                <?php 
                    if(authDriver::isLoggedin()){
                        echo '<li class="nav-item lishop"><a class="nav-link" href="/user/logout"><span><i class="bi bi-box-arrow-right"></i></a></li>';
                    }
                ?>


        </ul>
        </div>
    </div>
</nav>
