<?php
//session_start();
$actual_link = explode("/",$_SERVER[REQUEST_URI]);
if($actual_link[1]=="" || $actual_link[1] == "main"){
  $actual_link[1] = "main";
  }else{
  }
?>
<!DOCTYPE HTML>
<html> <!--archivo principal del servicio, se mandan a llamar los estilos css y js-->
    <head><meta charset="gb18030">
        
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>ARNESES</title>

        <link href="https://fonts.googleapis.com/css2?family=Bitter:wght@400;700&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="/static/css/bootstrap.min.css" >
        <link rel="stylesheet" href="/static/css/main.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">
        <?php
        $filr = './static/css/'.$actual_link[1].".css";
          if(file_exists($filr)){
            echo '<link rel="stylesheet" href="'.substr($filr,1).'">';
          }
        ?>

        
    </head>
   
    <body class='<?php echo $actual_link[1] ?>'>
        
        <?php
        
          templateDriver::content();
        ?>

  </body>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  <script src="/static/js/popper.min.js"></script>  
<script src="/static/js/bootstrap.min.js"></script>
<?php
    $filr = './static/js/'.$actual_link[1].".js";
    if(file_exists($filr)){
      echo '<script src="'.substr($filr,1).'"></script>';
    }

?>
  </html>